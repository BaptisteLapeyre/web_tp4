import React from 'react';
import './nav.css';

/* 
- import bootsrap dans le l'index
- import image dans public
- import Nav from './nav'; dans app js
*/

function App() {
    return (
        <nav className="navbar navbar-dark bg-dark">
            <a className="navbar-brand" href="#">
                <img src="./image/Banniere.PNG" width="90" height="30" className="d-inline-block align-top" alt="" />

                <p>League of Stones</p>
            </a>
            <form className="form-inline justify-content-end">
                <button class="btn btn-outline-secondary" type="button">Se connecter</button>
                <button class="btn btn-outline-secondary" type="button">S'inscrire</button>
            </form>
        </nav>
    );
}

export default App;

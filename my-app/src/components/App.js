import React from 'react';
import '../components/App.css';
import Nav from './nav';

function App() {
  return (
    <div className="App">
      <Nav></Nav>
    </div>
  );
}

export default App;
